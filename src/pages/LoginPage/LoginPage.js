import { Button, Checkbox, Form, Input } from "antd";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  setUserLogin,
  setUserLoginActionService,
} from "../../redux/slices/userSlice";
import { localStorageService } from "../../services/localStorageService";
import { userService } from "../../services/userService";

const LoginPage = () => {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    // userService
    //   .postDangNhap(values)
    //   .then((res) => {
    //     console.log(res);
    //     dispatch(setUserLogin(res.data.content));
    //     navigate("/");
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });

    // unwrap ~ handle dispatch thành promise
    dispatch(setUserLoginActionService(values))
      .unwrap()
      .then((resDispatch) => {
        console.log("resDispatch: ", resDispatch);
        localStorageService.setUserInfor(resDispatch);
        navigate("/");
      });
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="taiKhoan"
        rules={[
          {
            required: true,
            message: "Please input your username!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="matKhau"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        name="remember"
        valuePropName="checked"
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

export default LoginPage;
