import { message } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import HeaderResponsive from "../../components/Header/HeaderResponsive";
import SpinnerComponent from "../../components/SpinnerComponent/SpinnerComponent";
import { useWindowSize } from "../../Hook/useWindowSize";
import {
  setSpinnerEnded,
  setSpinnerStarted,
} from "../../redux/slices/spinnerSlice";
import { httpService } from "../../services/configURL";
import { userService } from "../../services/userService";
import TableUsers from "./TableUsers/TableUsers";

export default function UserManagePage() {
  const [userList, setUserList] = useState([]);
  const { isLoading } = useSelector((state) => state.spinnerSlice);
  const dispatch = useDispatch();
  let fetchUserList = () => {
    userService
      .getUserList()
      .then((res) => {
        let dataRaw = res.data.content.map((user) => {
          return {
            ...user,
            action: {
              onDetele: () => {
                userService
                  .deleteUser(user.taiKhoan)
                  .then((res) => {
                    message.success("Xoá thành công");
                    fetchUserList();
                  })
                  .catch((err) => {
                    message.error(err.response.data.content);
                  });
              },
              onEdit: () => {},
            },
          };
        });
        setUserList(dataRaw);
      })
      .catch((err) => {});
  };
  useEffect(() => {
    fetchUserList();
  }, []);
  let windowSize = useWindowSize();
  return (
    <div>
      <header className="text-red-500 text-3xl text-center">
        <HeaderResponsive />
      </header>
      <div className="container mx-auto">
        <TableUsers userList={userList} />
      </div>
    </div>
  );
}
