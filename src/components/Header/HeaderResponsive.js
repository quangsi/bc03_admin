import React from "react";
import { useWindowSize } from "../../Hook/useWindowSize";
import HeaderDesktop from "./HeaderDesktop";
import HeaderMobie from "./HeaderMobie";
import HeaderTablet from "./HeaderTablet";

export default function HeaderResponsive() {
  let windowSize = useWindowSize();

  let renderHeader = () => {
    if (windowSize.width > 992) {
      return <HeaderDesktop />;
    }
    if (windowSize.width > 768) {
      return <HeaderTablet />;
    } else {
      return <HeaderMobie />;
    }
  };
  return <div>{renderHeader()}</div>;
}
