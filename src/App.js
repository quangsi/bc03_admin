import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import UserManagePage from "./pages/UserManagePage/UserManagePage";
import DemoToolkitPage from "./pages/DemoToolkitPage/DemoToolkitPage";
import LoginPage from "./pages/LoginPage/LoginPage";
import SpinnerComponent from "./components/SpinnerComponent/SpinnerComponent";
// import { DemoReactScrool } from "./pages/DemoReactScrool/DemoReactScrool";

function App() {
  return (
    <div className="App">
      <SpinnerComponent />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<UserManagePage />} />
          <Route path="/toolkit" element={<DemoToolkitPage />} />
          {/* <Route path="/react-scrool" element={<DemoReactScrool />} /> */}
          <Route path="/login" element={<LoginPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
