import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { giamSoLuong, tangSoLuong } from "../../redux/slices/demoSlice";

export default function DemoToolkitPage() {
  let number = useSelector((state) => state.demoSlice.number);

  let dispatch = useDispatch();
  return (
    <div className=" container mx-auto py-40">
      <button
        onClick={() => {
          dispatch(tangSoLuong(10));
        }}
        className="px-5 py-2 rounded bg-blue-800 text-white"
      >
        Tăng
      </button>
      <span className="text-yellow-800 text-xl font-medium mx-5">{number}</span>
      <button
        onClick={() => {
          dispatch(giamSoLuong(1));
        }}
        className="px-5 py-2 rounded bg-red-800 text-white"
      >
        Giảm
      </button>
    </div>
  );
}
