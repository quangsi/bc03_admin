import { Space, Table, Tag } from "antd";
import { headerTableUser } from "../../../Utils/userMangement.utils";

const TableUsers = ({ userList }) => (
  <Table columns={headerTableUser} dataSource={userList} />
);

export default TableUsers;
