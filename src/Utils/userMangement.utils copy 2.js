import { Button, message, Tag } from "antd";
import { userService } from "../services/userService";

export const headerTableUser = [
  {
    title: "Họ tên",
    dataIndex: "email",
    key: "taiKhoan",
  },

  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
    render: (username) => {
      return <span className="text-blue-500">{username}</span>;
    },
  },

  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Loại người dùng",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
    render: (type) => {
      if (type == "QuanTri") {
        return <Tag color="lime">Quản Trị</Tag>;
      } else {
        return <Tag color="blue">  Khách hàng</Tag>;
      }
    },
  },
  {
    title: "Thao tác",
    dataIndex: "action",
    key: "action",
    render: ({ onDetele }, record) => {
      return (
        <>
          {" "}
          <Button type="primary">Sửa</Button>
          <Button
            onClick={() => {
              userService
                .deleteUser(record.taiKhoan)
                .then((res) => {
                  message.success("Xoá thành công");
                  onDetele();
                  console.log(res);
                })
                .catch((err) => {
                  message.error(err.response.data.content);
                  console.log(err);
                });
            }}
            type="primary"
            danger
          >
            Xoá
          </Button>
        </>
      );
    },
  },
];
// {
//     "taiKhoan": "abc123",
//     "hoTen": "Hoang Minh",
//     "email": "khongco@gmail.com",
//     "soDT": "090909090933",
//     "matKhau": "123456",
//     "maLoaiNguoiDung": "KhachHang",

// }
